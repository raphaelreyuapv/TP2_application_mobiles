package fr.uavignon.ceri.tp2.data;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import static fr.uavignon.ceri.tp2.data.BookRoomDatabase.databaseWriteExecutor;
public class BookRepository {
    private BookDao bookDao;
    private LiveData<List<Book>> allBooks;
    private MutableLiveData<Book> selectedBook = new MutableLiveData<>();

    public BookRepository(Application application){
        BookRoomDatabase db = BookRoomDatabase.getDatabase(application);
        bookDao = db.bookDao();
        allBooks = bookDao.getAllBooks();
    }

    public MutableLiveData<Book> getSelected(){
        return selectedBook;
    }

    public LiveData<List<Book>> getAllBooks(){
        return allBooks;
    }

    public void insertBook(Book book){
        databaseWriteExecutor.execute(() -> {
            bookDao.insertBook(book);
        });
    }

    public void deleteBook(long id){
        databaseWriteExecutor.execute(() -> {
            bookDao.deleteBook(id);
        });
    }

    public void getOneBook(long id) {
        Future<Book> fbooks = databaseWriteExecutor.submit(() -> {
            return bookDao.getOneBook(id);
        });
        try{
            selectedBook.setValue(fbooks.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    public void updateBook(Book newbook) {
        databaseWriteExecutor.execute(() -> {
            bookDao.updateBook(newbook);
        });
    }

}
