package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import fr.uavignon.ceri.tp2.data.Book;
import fr.uavignon.ceri.tp2.data.BookRepository;

public class DetailViewModel extends AndroidViewModel {
    private BookRepository repository;
    private MutableLiveData<Book> book;


    public MutableLiveData<Book> getBook(){
        return book;
    }
    public DetailViewModel(Application application){
        super(application);
        repository = new BookRepository(application);
    }
    public MutableLiveData<Book> getBook(long id){
        repository.getOneBook(id);
        return repository.getSelected();
    }
    public void setBook(long id){
        repository.getOneBook(id);
        this.book=repository.getSelected();
    }
    public void deleteBook(long id){
        repository.deleteBook(id);
    }
    public void insertOrUpdateBook(Book book){
        if(book.getId()==-1){
            Random rand=new Random();
            book.setId(rand.nextInt((999))+1);
            repository.insertBook(book);
        }else{
            repository.updateBook(book);
        }
    }
}
