package fr.uavignon.ceri.tp2;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.snackbar.Snackbar;

import fr.uavignon.ceri.tp2.data.Book;

public class DetailFragment extends Fragment {

    private EditText textTitle, textAuthors, textYear, textGenres, textPublisher;
    private Boolean newBook;
    private DetailViewModel viewModel;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);
        // Get selected book
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        textTitle = (EditText) view.findViewById(R.id.nameBook);
        textAuthors = (EditText) view.findViewById(R.id.editAuthors);
        textYear = (EditText) view.findViewById(R.id.editYear);
        textGenres = (EditText) view.findViewById(R.id.editGenres);
        textPublisher = (EditText) view.findViewById(R.id.editPublisher);

        if((int)args.getBookNum()!= -1){
            //le livre existe
            newBook=false;
            MutableLiveData<Book> book = viewModel.getBook((int)args.getBookNum());
            viewModel.setBook((int)args.getBookNum());
            if(viewModel.getBook()==null){
                Snackbar.make(view, "wtf " + (int)args.getBookNum(), Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }else{
            newBook=true;
        }
        listernerSetup();
        observerSetup();

        view.findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.tp2.DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });


    }

    private void listernerSetup(){
        View view=getView();
        view.findViewById(R.id.buttonUpdate).setOnClickListener(new View.OnClickListener(){
            @Override
                    public void onClick(View view){
                if(!newBook){
                    viewModel.getBook().observe(getViewLifecycleOwner(),
                              book -> {
                                  long id;

                                  id = book.getId();
                                  String title = String.valueOf(textTitle.getText());
                                  String author = String.valueOf(textAuthors.getText());
                                  String year = String.valueOf(textYear.getText());
                                  String genres = String.valueOf(textGenres.getText());
                                  String publish = String.valueOf(textPublisher.getText());
                                  Book updateBook = new Book(title, author, year, genres, publish);
                                  updateBook.setId(id);
                                  viewModel.insertOrUpdateBook(updateBook);
                            });
                }else{
                    String title = String.valueOf(textTitle.getText());
                    String author = String.valueOf(textAuthors.getText());
                    String year = String.valueOf(textYear.getText());
                    String genres = String.valueOf(textGenres.getText());
                    String publish = String.valueOf(textPublisher.getText());
                    Book newBook = new Book(title,author,year,genres,publish);
                    newBook.setId(-1);
                    viewModel.insertOrUpdateBook(newBook);
                }
            }
        });
    }

    private void observerSetup(){
        if(!newBook){
            viewModel.getBook().observe(getViewLifecycleOwner(),
                    new Observer<Book>(){
                @Override
                        public void onChanged(Book book){
                        textTitle.setText(book.getTitle());
                        textAuthors.setText(book.getAuthors());
                        textYear.setText(book.getYear());
                        textGenres.setText(book.getGenres());
                        textPublisher.setText(book.getPublisher());
                }
                    });
        }
    }
}